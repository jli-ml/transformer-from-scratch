import torch
import torch.nn as nn
import math

from src.model.input_embeddings import InputEmbeddings

# https://medium.com/@hunter-j-phillips/positional-encoding-7a93db4109e6

class PositionalEncoding(nn.Module):
    def __init__(self, d_model: int, seq_len: int, dropout: float) -> None:
        super().__init__()
        self.d_model = d_model
        self.seq_len = seq_len  # max length of sentence
        self.dropout = nn.Dropout(dropout)  # for less overfitting

        # Positional encoding is a matrix with shape of seq_len x d_model
        # Create a matrix of shape (seq_len, d_model)
        pe = torch.zeros(seq_len, d_model)

        # Create a vector of shape (seq_len, 1)
        position = torch.arange(0, seq_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, d_model, 2).float() * (-math.log(10000.0)/d_model))
        # Apply the sin to even positions (even index position inside the word vector; not the position in the sentence)
        # position * div_term is a broadcast operation
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)

        # add batch dimension
        pe = pe.unsqueeze(0)  # (1, seq_len, d_model)

        self.register_buffer('pe', pe)

    def forward(self, x):
        x = x + (self.pe[:, :x.shape[1], :]).requires_grad_(False)  # it is not learned
        return self.dropout(x)