import torch
import torch.nn as nn

class LayerNormalization(nn.Module):
    def __init__(self, eps: float = 10**-6):
        super().__init__()
        self.eps = eps  # to avoid divide by zero
        self.alpha = nn.Parameter(torch.ones(1))  # multiplied; learnable parameter
        self.bias = nn.Parameter(torch.zeros(1))  # added; learnable parameter


    def forward(self, x):
        mean = x.mean(dim=-1, keepdim=True)  # dim=-1 for last dimension => mean of item
        std = x.std(dim=-1, keepdim=True)
        return self.alpha * (x - mean) / (std + self.eps) + self.bias


