import torch
import torch.nn as nn
import math

class InputEmbeddings(nn.Module):

    def __init__(self, d_model: int, vocab_size: int):
        super().__init__()
        self.d_model = d_model
        self.vocab_size = vocab_size
        # just a mapping between numbers and vector of size d_model, same number gets the same vector everytime
        self.embedding = nn.Embedding(vocab_size, d_model)

    def forward(self, x):
        return self.embedding(x) / math.sqrt(self.d_model)


# if __name__=="__main__":
#     embedding = InputEmbeddings(6, 10)
#     e = embedding(torch.LongTensor([1])) # embedding for index (ID) 1
#     print(e)




