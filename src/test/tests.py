import torch
import torch.nn as nn

# transpose
t = torch.Tensor([[1,2], [3,4]])
t = t.transpose(0,1)
print(t)

# matrix multiplication @
t = torch.Tensor([[1,2], [3,4]])
p = torch.Tensor([[1,2], [3,4]])
print(t @ p)

# masked_fill_
mask = torch.Tensor([[1, 0], [1,0]])
t = torch.Tensor([[1,2], [3,4]])
t.masked_fill_(mask==1, 999)
print(t)

# softmax
t = torch.Tensor([[1,2], [3,1]])
t = t.softmax(dim=-1)
print(t)


# dropout
dropout = nn.Dropout(0.25)
t = torch.Tensor([[1,2], [3,4]])
t = dropout(t)
print(t)

# view
a = torch.tensor([1. ,2. ,3. ,4. ,5. ,6.])
print(a)
# the size -1 is inferred from other dimensions
# find a view of the above tensor
b = a.view(-1,2)
print(b)

